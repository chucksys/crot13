CC=gcc
CFLAGS=-O2

all: rot13

rot13: rot13.c
	${CC} ${CFLAGS} $< -o $@
